
// ### Navigation bar
// As there are no page number on the Index of the book,
// on the side of the book pages we want to add a dark bar that indicates the article name.
// As the book progress towards its end, the bar lowers its position
// For the pages of an article, the bar remains in the same position
// It  shall include the article name
// #### How to?
// * Only on the right pages

// `div.article#Property:Person .navigationbar{ margin-top:0; }`

// `div.article#Introduction .navigationbar{ margin-top:5%; }`

// `...`

// `margin-top:` will be have to be devided by the number of articles that are on the index.

function navbar(){
    var articles  = $('div.article'); // get all div.article 
    var articles_n = articles.lenght;// number of articles
    
    var height = $('body').css('height'); // find page height; // how to find height in @page at-rule? 
    // Note: @page {
    //     size: 20cm 26cm;/* width height */
    //     margin-top:11%;
    //     margin-bottom: 22%;
    // }

    var step = height/articles_n; 
    console.log(height, step); 
    // steps: divide page height by number of articles
    // at each article move the bar down 1 step (css)
}


$(document).ready(
    function(){
    navbar();
    }
)