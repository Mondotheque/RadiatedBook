#! /usr/bin/env python
# -*- coding: utf-8 -*-

from mwclient import Site

from urlparse import urlparse
from argparse import ArgumentParser
from time import mktime
from datetime import datetime
import os, urllib
import pprint

####
# Arguments
####
p = ArgumentParser()
p.add_argument("--host", default="www.mondotheque.be")
p.add_argument("--path", default="/wiki/", help="nb: should end with /")
p.add_argument("--category", "-c", nargs="*", default=[["Publication"]], action="append", help="category to query, use -c foo -c bar to intersect multiple categories")

p.add_argument("--download", "-d", default="htmlpages", help="What you want to download from wiki: htmlpages; images; wikipages")

args = p.parse_args()
#print args

#########
# defs
#########
def mwsite(host, path): #returns wiki site object
    site = Site(('http',host), path)
    return site

def mw_cats(site, categories): #returns pages member of categories
    last_names = None
    for cat in categories:
            for ci, cname in enumerate(cat):
                    cat = site.Categories[cname]
                    pages = list(cat.members())
                    if last_names == None:
                            results = pages
                    else:
                            results = [p for p in pages if p.name in last_names]                
                    last_names = set([p.name for p in pages])
            results = list(results)
    return [p.name  for p in results]


def mw_page(site, page):
    page = site.Pages[page]
    return page

def mw_page_imgsurl(site, page, imgdir):
    #all the imgs in a page
    #returns dict {"imagename":{'img_url':..., 'timestamp':... }} 
    imgs = page.images() #images in page
    imgs = list(imgs) 
    imgs_dict = {}
    if len(imgs) > 0:
        for img in imgs:
            if 'url' in img.imageinfo.keys(): #prevent empty images
                imagename = img.name
                imagename = (imagename.capitalize()).replace(' ','_')
                imageinfo = img.imageinfo
                imageurl = imageinfo['url']
                timestamp = imageinfo['timestamp'] ## is .isoformat() 
                imgs_dict[imagename]={'img_url': imageurl,
                                    'img_timestamp': timestamp } 
                os.system('wget {} -N --directory-prefix {}'.format(imageurl, imgdir))
                # wget - preserves timestamps of file
                # -N the decision as to download a newer copy of a file depends on the local and remote timestamp and size of the file.                  
        return imgs_dict
    else:
        return None


def write_plain_file(content, filename):
    edited = open(filename, 'w') #write
    edited.write(content)
    edited.close()

# ------- Action -------

site = mwsite(args.host, args.path)
html_dir="texts-html"
img_dir="{}/imgs".format(html_dir)

memberpages=mw_cats(site, args.category)
memberpages.append('The radiated book') #Append book index
memberpages.append('Special:Disambiguation')
#print 'memberpages from', args.category, ':', memberpages
memberpages
for pagename in memberpages:
    #print args.download
    if args.download == 'htmlpages':
        url = u"http://www.mondotheque.be/wiki/index.php?action=render&title="+pagename
        url= url.encode('utf-8')
        # if 'Special:' not in page:
        #     pageurl = u"http://www.mondotheque.be/wiki/index.php/"+page

        response = urllib.urlopen(url) # should urllib be replace by wget, as it check timecodes and sizes of files before downloading?
        print 'Download html source from', pagename
        htmlsrc = response.read()
        pagename = (pagename.replace(" ", "_")).encode('utf-8')
        write_plain_file(htmlsrc, "{}/{}.html".format(html_dir, pagename.replace('/','_')) )

    elif args.download == 'images':
        print 'Download images from', pagename
        page = mw_page(site, pagename)
        page_imgs = mw_page_imgsurl(site, page, img_dir)



    elif args.download == 'wikipages':
        print  'NOT IMPLEMENTED'
